#!/usr/bin/python

from yaml import safe_load


def is_wildcard(symbol_f: str, config_yaml: dict) -> bool:
    for reel in config_yaml.get("reels"):
        for reel_symbol in reel:
            symbol_is_wildcard = reel_symbol.get("symbol") == "wildcard"
            if reel_symbol.get("symbol") == symbol_f and symbol_is_wildcard:
                return True
    return False


def get_symbol_count(symbol_f: str, reel_number: int, config_yaml: dict) -> int:
    reels_f = config_yaml.get("reels")

    reel = reels_f[reel_number]

    for item in reel:
        if item.get("symbol") == symbol_f:
            wildcard_count = 0
            if not is_wildcard(symbol_f, config_yaml):
                for item_w in reel:
                    if item_w.get("symbol") == "wildcard":
                        wildcard_count += item_w.get('count')

            return item.get("count") + wildcard_count

    return 0


def total_combos_count(config_yaml: dict):
    reels_f = config_yaml.get("reels")
    total=1
    for reel in reels_f:
        reel_total_symbols=0
        for item in reel:
            reel_total_symbols+=count
        total*=reel_total_symbols
    return total

with open("input.yaml", "r") as f:
    config: dict = safe_load(f)

combo_counts: list = []
combos: list = config.get("combos")
reels: list = config.get("reels")

for combo in combos:
    symbols: list = combo.get("symbols")
    combo_count = 1
    for i in range(len(reels)):
        if i >= len(symbols):
            total_count = 0
            for j in range(len(reels[i])):
                total_count += reels[i][j].get('count')
            combo_count *= total_count
            break
        symbol = symbols[i]
        count = get_symbol_count(symbol, i, config)
        combo_count *= count
    if all(map(lambda sym: not is_wildcard(sym, config), symbols)):
        wildcard_combo_count = 1
        for j in range(len(reels)):
            wildcard_combo_count *= get_symbol_count("wildcard", j, config)
        combo_count -= wildcard_combo_count
    combo_counts.append(combo_count)

wpcs: list = []
for i in range(len(combos)):
    wpc = combo_counts[i] * combos[i].get('prize')
    wpcs.append(wpc)

total_wpc = sum(wpcs)

total_combos = total_combos_count(config)

individual_rtps: list = []

for i in range(len(combos)):
    wpc = combo_counts[i] * combos[i].get('prize')
    individual_rtp = (wpc / total_combos) * 100
    individual_rtps.append(individual_rtp)
    print(
        f"Symbols: {combos[i].get('symbols')} Combo Count:{combo_counts[i]} Prize:{combos[i].get('prize')} WPC:{wpc} Individual RTP:{individual_rtp}")

total_rtp = sum(individual_rtps)

print(f"Total RTP:{total_rtp}")
